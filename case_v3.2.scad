/* SPDX-License-Identifier: MIT */
// Copyright (c) 2023 Janne Grunau

// tray style case for Central Scrutinizer v3.2

$fn=60;

// layer resolution for bottom chamfer
lh = 0.1;
// height of the case base
base_h = 1.2;

// height of the case without base
height = 6;
// wall thickness
wt = 1.8;

// Cutouts for USB-C/micro USB connectors
gap_c = 14 + wt;
// make the case symmetric
gap_m = gap_c; //12 + wt;

// M2 tapping bore, assume this approximation works well enough for
// self-"tapping" with regular M2 machine screws.
m2_bore = 1.85; // tapping diameter would be 1.6

// board size
size = [53, 33];
// size of the mounting hole pattern
h_pos = [38.1, 27.9];


module mirror_copy(v = [0, 1, 0]) {
    union() {
        children();
        mirror(v) children();
    }
}

module connector_cutout(width, off=0) {
    mirror_copy() translate([off, off, 0]) union() {
        difference() {
            translate([-wt/2 - 0.1, (width - wt)/4 - off]) square([wt + 0.2, (width - wt) / 2 + 2 * off], center=true);
            translate([-wt/2, (width) / 2 - wt]) square([wt / 2, wt / 2]);
        }
        translate([-wt/2, (width)/2-wt]) circle(d=wt);
        translate([-wt/2, width/2]) difference() {
            translate([-wt/4 - 0.1, -wt/4]) square([wt / 2 + 0.2, wt / 2], center=true);
            circle(d=wt);
        }
    }
}

module standoff(st_h) {
    difference() {
        union() {
            cylinder(d=5, h=st_h);
            translate([0, (size.y - h_pos.y - wt) / 2, st_h / 2]) cube([5, (size.y - h_pos.y) - wt, st_h], center=true);
        }
        cylinder(d=m2_bore, h=st_h + 0.1);
        translate([0, 0, st_h - 0.5]) cylinder(d1=m2_bore, d2=m2_bore + 0.5, h=0.51);
    }
}

union() {
    // main case without bottom
    difference() {
        hull() {
            for (x_pos = [-size.x, size.x], y_pos = [-size.y, size.y]) {
                translate([x_pos / 2, y_pos / 2, 0]) cylinder(r=wt, h=height);
            }
        
        }
        translate([0, 0, height / 2 + 0.049]) {
            translate([0, 0, 0])         cube([size.x, size.y, height+0.1], center=true);
            translate([-size.x/2, 0, 0]) cube([2 * wt + .1, gap_c, height+0.1], center=true);
            translate([ size.x/2, 0, 0]) cube([2 * wt + .1, gap_m, height+0.1], center=true);
        }
    }
    // add rounded corners to usb-c / micro usb cutouts
    mirror_copy() translate([-(size.x + wt) / 2,  gap_c / 2, 0]) cylinder(d=wt, h=height);
    mirror_copy() translate([ (size.x + wt) / 2,  gap_m / 2, 0]) cylinder(d=wt, h=height);

    // add 4 standoffs to mount the Central Scrutinizer board to
    for (xoff = [-h_pos.x, h_pos.x], yoff = [-h_pos.y, h_pos.y])
    {
        translate([xoff/2, yoff/2, 0]) 
        rotate([0, 0, (yoff < 0.0) ? 180 : 0]) standoff(4);
    }
    
    // add case bottom with chamfer and partial cutouts for the usb connectors
    mirror([0, 0, 1])  difference() {
        // base plate with chamfers
        union() for (z_off = [0:lh:base_h]) {
            translate([0, 0, z_off]) linear_extrude(height = lh) hull() {
                lsize = [size.x - z_off, size.y - z_off];
                for (x_pos = [-lsize.x, lsize.x], y_pos = [-lsize.y, lsize.y]) {
                    translate([x_pos / 2, y_pos / 2]) circle(r=wt);
                }
            }
        }
        // chamfered cutouts for usb connectors
        union() for (z_off = [0:lh:base_h]) {
            translate([-size.x/2, 0, z_off]) linear_extrude(height = lh) connector_cutout(gap_c, z_off/2);
            translate([ size.x/2, 0, z_off]) linear_extrude(height = lh) mirror([1,0,0]) connector_cutout(gap_m, z_off/2);
        }
    }
}
