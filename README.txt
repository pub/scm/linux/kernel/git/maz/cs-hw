This is a KiCad project allowing to control an Apple M1/M2. It started
its life as "m1-ubmc", but its real name is "Central Scrutinizer".

Basically a FUSB302, a couple of level shifters and a RPi Pico. The
project has the component references for JLCPCB except for the Pico,
and is (at the time of writing), pretty cheap to build. Gives you
serial, reboot control and USB passthrough over a micro-USB connector.

Someone who knows what they are doing could surely do much better. It
works well enough for me, but please tell me if you spot anything
wrong -- this is my first PCB design in about 30 years.

For the licencing information, see the LICENSE file in the repo, and
interpret "software" in a liberal manner...

* Revisions:

Each produced revision corresponds to a tag:

- v0: First produced version. Ignore it, it's a dud. Yes, it is hard
  to tell TX from RX. And a wrong resistor on the 1.2v divider (bad
  LCSC reference). Live and learn. Nothing that a couple of drilled
  vias, some magnet wire and a 1/4W resistor can't fix anyway. At
  least I knew what to fix.

- v1: It's alive! Works as well as expected.

- v2: Dual CC connection, supports SBU pin swapping. Fully
  functionnal, but only a stepping stone towards v3.

- v3: Supports USB2.0 D+/D- being routed as serial lines, meaning you
  can use cheap USB-C cables as long as you don't need USB2.0
  pass-through. Also comes with UART1 pins routed out, which could be
  used as non-USB control channel (old fashioned console server, for
  example).

  Be careful though, the USB switches used here are so small that
  JLCPCB couldn't properly solder them, resulting in an embarrassing
  80% defect rate... You've been warned.

- v3.1: Exactly the same as v3, only with switches that are physically
  larger, meaning the assembly is much easier.

- v3.2: Limited improvements on v3.1:
  * a 1.2v LDO as the supply for the level shifters (instead of the
    simplistic voltage divider),
  * Molex connectors instead of the LCSC-supplied connectors that are
    hard to source anywhere else,
  * Nexperia's 74AVC1T45 instead of Diode's, for smaller footprint and
    lower minimal voltage
  * ... and a new silkscreen, which makes everything better!

Anything else is probably even worse than the above.

In general, stick to something that is on the 'master' branch. Major
changes always happens on a separate branch, which ultimately gets
merged on the trunk. I may push the odd fix directly on master, but it
should never be something that meaningfully impacts the design.

* Building your own:

This is the preferred option, really. I've used JLCPCB for all the
revisions above, both good and bad. If you're not good at soldering
very small stuff, get them to do the heavy lifting. The process is
very straightforward, but you will have to produce at least 5
boards. Find some fellow hackers and share the costs!

The 'production' directory contain the Gerber files in a single ZIP,
BOM and positions in CSV format (produced by the JLCPCB fabrication
toolkit plugin). You only need to upload those to the JLCPCB website,
check the orientation of the components, and let it rip. I've used the
basic FR-4 with HASL finish, Economic PBCA type, and the result is OK.

Things to be aware of:

- The assembly side is at the *bottom* of the board. It is only the
  Pico that goes on the top side. Make sure you pick the bottom side
  in the web interface.

- Warning about JP[1-8] being absent from the position file can be
  safely ignored. The web interface gets confused about having pads
  without anything soldered to them.

- Components sometimes are out of stock. While you can often quickly
  find a direct replacement in the LCSC library, be very careful about
  the footprint, specially with connectors. You may have to amend the
  PCB design and regenerate the production files.

- The orientation bit is absolutely crucial. While I do my best to
  reconcile what KiCad and JLCPCB respectively think of the
  orientation of components by adding rotation offsets to the
  metadata, you absolutely need to check this carefully before
  starting the build. Look for the orientation markers on the PCB and
  use the web editor to align the component positional markers with
  them. Do not expect JLCPCB to catch these mistakes for you.

Of course, JLCPCB isn't the only game in town, and their QC is dodgy
at best. If you know of a decent PCB+assembly shop and can help with
making the PCB easy to get produced, let me know. I'm happy to add
metadata to the schematic to drive a fabrication plugin/framework if
there is one.

Ideally, I'd like to have a collection of setups that allow people to
build boards locally instead of having shipped around the world...

You can also use one of these PCB shops to only produce the PCB and
populate the board yourself. If you are in this category, I assume you
know what you are doing and you need no further advice from me!

* Final assembly:

Once you have managed to get your hands on an populated CS board, you
must solder a Pico to it. Make sure that:

- the two boards are back to back (all components are on the outside
  of the board sandwich).

- the two micro-USB connectors on the same end of the assembly -- if
  you have the USB-C and Pico micro-USB close to each other, you're
  doing it wrong.

- the two boards are far apart enough that the two micro-USB
  connectors can be plugged without interfering with one another.
  I've used male turned pin strips as the connector, and they are
  great.  Normal header pins are also fine if you're not bothered with
  one side sticking out more than it normally should. You may need to
  add extra spacers to keep the boards apart.

- you use a bog standard RPi Pico. Not a Pico W, not a one of the many
  variants with a creative pinout... It may work, it may not. Be
  cheap, don't use anything fancy. The Pico W is known to have a
  different GPIO assignment, which interferes with the current
  SW. Nothing terrible, but enough to spend some time debugging it.

* Dual board configuration:

So you have a pair of Apple machines, two CS boards, but only a single
Pico? Guess what, that's everything you need. Each CS board can use
two different configurations that can work together:

- the default configuration is to use UART0, I2C0, and a defined set
  of GPIOs,

- and there is an alternate setup using UART1, I2C1, and another set
  of GPIOs.

v0 and v1 use a set of 0R resistors that need to be painfully moved
(see the schematics to identify the resistors and their landing spot).
v2+ use a set of PCB jumpers that are easier to modify: for each of
JP1 to JP8, cut the trace between pads 1 and 2, and place a blob of
solder across pads 2 and 3.

To assemble the whole thing, I use long wrapping connectors that allow
all three boards (one Pico and 2 CS) to be stacked. The SW will
automatically detect which board is present and use the right
configuration.

* Software:

For the SW that runs on the RPi Pico and how to use the damn thing,
head to 

https://git.kernel.org/pub/scm/linux/kernel/git/maz/cs-sw.git

* Case:

case_v3.2.scad contains an OpenSCAD design for a tray case.
The model file is available on
https://www.printables.com/model/585592-central-scrutinizer-case

* Testing jig

The 'jig' directory contains a very simple PCB that can be used as the
basis for a testing jig.

Use two identical boards, where the Pico gets plugged into the first,
and the CS board to be tested plugs into the second using pogo
pins. Link the two boards using the 40 pins in the middle.

You can chose to connect all the 40 signals using pogo pins, or just
pick the setup you want. I found it convenient to build two jigs, one
for the default CS configuration, and one for the alternate one.
