EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_USB:FE1.1s U?
U 1 1 62EE4897
P 9250 2650
F 0 "U?" H 9250 1461 50  0000 C CNN
F 1 "FE1.1s" H 9250 1370 50  0000 C CNN
F 2 "Package_SO:SSOP-28_3.9x9.9mm_P0.635mm" H 10300 1150 50  0001 C CNN
F 3 "https://cdn-shop.adafruit.com/product-files/2991/FE1.1s+Data+Sheet+(Rev.+1.0).pdf" H 9250 2650 50  0001 C CNN
	1    9250 2650
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_C_Receptacle J?
U 1 1 62EE7E62
P 1750 3000
F 0 "J?" H 1857 4267 50  0000 C CNN
F 1 "USB_C_Receptacle" H 1857 4176 50  0000 C CNN
F 2 "" H 1900 3000 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 1900 3000 50  0001 C CNN
	1    1750 3000
	1    0    0    -1  
$EndComp
$Comp
L 2022-08-06_12-04-56:SC090813 U?
U 1 1 62EE6053
P 5700 850
F 0 "U?" H 7000 1337 60  0000 C CNN
F 1 "SC090813" H 7000 1231 60  0000 C CNN
F 2 "IC57_RP2040" H 7000 1190 60  0001 C CNN
F 3 "" H 5700 850 60  0000 C CNN
	1    5700 850 
	1    0    0    -1  
$EndComp
$Comp
L 2022-08-06_12-04-56:SC090813 U?
U 2 1 62EE78A3
P 4350 5500
F 0 "U?" H 5678 4953 60  0000 L CNN
F 1 "SC090813" H 5678 4847 60  0000 L CNN
F 2 "IC57_RP2040" H 5650 5840 60  0001 C CNN
F 3 "" H 4350 5500 60  0000 C CNN
	2    4350 5500
	1    0    0    -1  
$EndComp
$Comp
L Interface_USB:FUSB302BMPX U?
U 1 1 62EE330E
P 4400 2250
F 0 "U?" H 4400 1669 50  0000 C CNN
F 1 "FUSB302BMPX" H 4400 1760 50  0000 C CNN
F 2 "Package_DFN_QFN:WQFN-14-1EP_2.5x2.5mm_P0.5mm_EP1.45x1.45mm" H 4400 1750 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/FUSB302B-D.PDF" H 4500 1850 50  0001 C CNN
	1    4400 2250
	-1   0    0    1   
$EndComp
$EndSCHEMATC
